README for Paul Yang CMB Meet the team submission

1)  I used some cocoapods packages.  To run the project, please open CMB_MeetTheTeam.xcworkspace instead of the .xcodeproj file.  
2)  Swiping left or right goes on to the next card.
3)  Tap on a team member's card to view their details.
4)  In the detail view, swipe down to dismiss the view.  

Packages used:
1)  I used the KolodaView swipe card interface in order to display the various profiles.  Apologies for any resemblance to competing apps.  If you prefer a tableview implementation, I can write the code for that as well.
2)  KingFisher module used for loading images using url without having to manually download the image.  However, I did not have time to get the correct aspect ratios for non-square images.

Comments or questions?  Email me at mjpablo23@gmail.com

Thanks,
Paul Yang


