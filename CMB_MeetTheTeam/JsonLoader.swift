//
//  JsonLoader.swift
//  CMB_MeetTheTeam
//
//  Created by Paul Yang on 9/11/18.
//  Copyright © 2018 Paul Yang. All rights reserved.
//

import Foundation
import UIKit

// loads the json into a file, and keeps array of TeamMember objects
// test 

class JsonLoader:NSObject {
    
    static let shared = JsonLoader()
    
    // team member array
    // team members will be accessed from this array when needed
    var teamMembers:[TeamMember]! = [TeamMember]()
    
    override init() {
        print("JsonLoader init")
    }
    
    func loadTeamFromJsonFile() {
        // get path for file using Bundle
        let path = Bundle.main.path(forResource: "team", ofType: "json")
        
        do {
            // get data from file
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            
            // serialize data into a json object
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: [])
            
            // cast jsonResult as array
            let dataArray = jsonResult as? NSArray


            // loop through the team member json objects
            for data in dataArray!  {
                
                // make each team member json object into dictionary
                let info = data as! NSDictionary
                
                // extract each field from dictionary and write into string
                let id = info["id"] as! String
                let name = info["name"] as! String
                let position = info["position"] as! String
                let profile_image = info["profile_image"] as! String
                let personality = info["personality"] as! String
                let interests = info["interests"] as! String
                let dating_preferences = info["dating_preferences"] as! String
                
                // instantiate a TeamMember object
                let teamMember:TeamMember = TeamMember(id: id, name: name, position:position, profile_image:profile_image, personality:personality, interests:interests, dating_preferences:dating_preferences)

                // print each field 
                print(teamMember.getId())
                print(teamMember.getName())
                print(teamMember.getProfileImage())
                print(teamMember.getName())
                print(teamMember.getInterests())
                print(teamMember.getDatingPreferences())
                
                // append the teamMember to the teamMembers array
                teamMembers.append(teamMember)
            }
            
            // print number of team members
            print("number of team members: \(teamMembers.count)")
            
        
        } catch {
        
        }
    }
}
