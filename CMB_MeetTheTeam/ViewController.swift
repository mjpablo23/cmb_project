//
//  ViewController.swift
//  CMB_MeetTheTeam
//
//  Created by Paul Yang on 9/11/18.
//  Copyright © 2018 Paul Yang. All rights reserved.
//

import UIKit
import Koloda
import pop
import Kingfisher

// variables for koloda view swipe card controller 
private let frameAnimationSpringBounciness: CGFloat = 9
private let frameAnimationSpringSpeed: CGFloat = 16
private let kolodaCountOfVisibleCards = 2
private let kolodaAlphaValueSemiTransparent: CGFloat = 0.1

// this is the main view controller

class ViewController: UIViewController {

    // outlet to storyboard kolodaView under ViewController
    @IBOutlet weak var kolodaView: CustomKolodaView!
    
    // width and height
    var screenHeight:CGFloat! = 0
    var screenWidth:CGFloat! = 0
    
    // team member clicked
    var teamMemberClicked:TeamMember!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        JsonLoader.shared.loadTeamFromJsonFile()
        
        print("number of team members: \(JsonLoader.shared.teamMembers.count)")
        
        screenWidth = UIScreen.main.bounds.width
        screenHeight = UIScreen.main.bounds.height
        
        // variables for swipe card view 
        kolodaView.alphaValueSemiTransparent = kolodaAlphaValueSemiTransparent
        kolodaView.countOfVisibleCards = kolodaCountOfVisibleCards
        kolodaView.delegate = self
        kolodaView.dataSource = self
        kolodaView.animator = BackgroundKolodaAnimator(koloda: kolodaView)
        
        view.addSubview(kolodaView)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let vc = segue.destination as? DetailsViewController {
            vc.teamMember = teamMemberClicked
        }
    }
}


//MARK: KolodaViewDelegate
extension ViewController: KolodaViewDelegate {
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        kolodaView.resetCurrentCardIndex()
    }
    
    func koloda(_ koloda: KolodaView, allowedDirectionsForIndex index: Int) -> [SwipeResultDirection] {
        return [.left, .right]
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        print("clicked card for index: \(index)")
        teamMemberClicked = JsonLoader.shared.teamMembers[index]
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "details", sender: self)
        }
    }
    
    func koloda(_ koloda: KolodaView, draggedCardWithPercentage finishPercentage: CGFloat, inDirection direction: SwipeResultDirection) {
        
    }
    
    // MARK: -- swipe -- calls like, email, or pass
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        //print("swiped in direction \(direction)")
        
        if (direction.rawValue == "right") {
            //print("did swipe right")
            swipedRight()
        }
        else if (direction.rawValue == "left") {
            //print("did swipe left")
            swipedLeft()
        }
        else if (direction.rawValue == "up") {
            //print("did swipe up")
            swipedUp()
        }
    }
    
    func swipedRight() {

    }
    
    func swipedLeft() {
    }
    
    func swipedUp() {
    }

    func kolodaShouldApplyAppearAnimation(_ koloda: KolodaView) -> Bool {
        return true
    }
    
    func kolodaShouldMoveBackgroundCard(_ koloda: KolodaView) -> Bool {
        return true
    }
    
    func kolodaShouldTransparentizeNextCard(_ koloda: KolodaView) -> Bool {
        return true
    }
    
    func koloda(kolodaBackgroundCardAnimation koloda: KolodaView) -> POPPropertyAnimation? {
        let animation = POPSpringAnimation(propertyNamed: kPOPViewFrame)
        animation?.springBounciness = frameAnimationSpringBounciness
        animation?.springSpeed = frameAnimationSpringSpeed
        return animation
    }
    
}


//MARK: KolodaViewDataSource
extension ViewController: KolodaViewDataSource {
    
    public func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return DragSpeed.default
    }
    
    public func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return JsonLoader.shared.teamMembers.count
    }
    
    // this method DOES get called for the last koloda card in the set.
    func koloda(_ koloda: KolodaView, didShowCardAt index: Int) {

    }
    
    @objc func doneDownloadingPage() {
        
    }
    
    // MARK: -- view for card at index
    public func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        
        let teamMemberForCard:TeamMember = JsonLoader.shared.teamMembers[index]
        
        // get the urlString
        let urlString:String! = teamMemberForCard.getProfileImage()
        
        let cardImageView = makeKolodaCardWithText(viewController:self, urlString:urlString, detailsText:teamMemberForCard.getShortDetailsText(), screenWidth:screenWidth, screenHeight:screenHeight)
        
        return cardImageView
    }
    
    func makeKolodaCardWithText(viewController:UIViewController, urlString:String, detailsText:String, screenWidth:CGFloat, screenHeight:CGFloat) -> UIImageView {
        let url = URL(string: urlString)
        
        let squareImageView:UIImageView! = UIImageView(frame:CGRect(x: 0, y: 0, width: screenWidth-20, height: screenWidth-20))
        // maintain aspect ratio
        squareImageView.contentMode = .scaleAspectFill
        squareImageView.kf.setImage(with: url)
        
        Utils.shared.setViewToRoundedNoBorder(view: squareImageView)
        
        // long image, height is almost length of screen
        let longImageView:UIImageView! = UIImageView(frame: CGRect(x: 0, y: 0, width: screenWidth - 20, height: screenHeight - 80))
        longImageView.image = UIImage(named: "cyanOrange.png")
        
        Utils.shared.setViewToRoundedNoBorder(view: longImageView)
        longImageView.addSubview(squareImageView)
        
        let labelIndex:UILabel! = UILabel(frame: CGRect(x: 10, y: squareImageView.frame.maxY + 10, width: screenWidth - 30, height: 150))
        labelIndex.numberOfLines = 0
        labelIndex.attributedText = Utils.shared.convertToAttributedString(str: detailsText)
        
        longImageView.addSubview(labelIndex)
        
        let viewFrameWidth:CGFloat! = viewController.view.frame.width - 20
        
        var cardFrame:CGRect!
        var cardImageView:UIImageView!
        
        cardFrame = CGRect(x: 0, y: 0, width: viewFrameWidth, height: viewFrameWidth + 50)
        cardImageView = UIImageView(frame: cardFrame)
    
        cardImageView.addSubview(longImageView)
        
        return cardImageView
    }
    

    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        return OverlayView()
    }
    

    func callRevert() {
        //        self.kolodaView?.revertAction()
    }
    
}
