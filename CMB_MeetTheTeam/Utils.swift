//
//  Utils.swift
//  CMB_MeetTheTeam
//
//  Created by Paul Yang on 9/11/18.
//  Copyright © 2018 Paul Yang. All rights reserved.
//

import Foundation
import UIKit

// Utils class contains utility functions

class Utils:NSObject {
    static let shared = Utils()
    
    override init() {
        print("init")
    }
    
    // rounds a view's corners so it looks nicer
    func setViewToRoundedNoBorder(view: UIView) {
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
    }

    
    // converts a string to NSMutableAttributedString with AvenirNext front
    func convertToAttributedString(str: String) -> NSMutableAttributedString {
        let myMutableString = NSMutableAttributedString(
            string:  str,
            attributes: [NSAttributedStringKey.font:UIFont(
                name: "AvenirNext-Regular", 
                size: 18.0)!])
        return myMutableString
    }
}
