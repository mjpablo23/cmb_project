//
//  DetailsViewController.swift
//  CMB_MeetTheTeam
//
//  Created by Paul Yang on 9/11/18.
//  Copyright © 2018 Paul Yang. All rights reserved.
//

// detailed card view for after clicking on someone, to see their details

import UIKit

class DetailsViewController: UIViewController, UIScrollViewDelegate {
    
    // the team member being shown
    var teamMember:TeamMember!
    
    // profile image view
    var profileImage:UIImageView! = UIImageView()
    
    // photo url
    var photoUrl:String! = ""
    
    // details to be displayed in text view
    var detailsTextView:UITextView! = UITextView()
    
    // screen height and width
    var screenHeight:CGFloat! = 0
    var screenWidth:CGFloat! = 0
    
    // scrollview containing the image
    var scrollView: UIScrollView! = UIScrollView()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        screenWidth = UIScreen.main.bounds.width
        screenHeight = UIScreen.main.bounds.height
        
        scrollView.delegate = self
        
        // initialize the scrollview with imageView and text view
        initializeScrollView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: -- view initialization
    // initialize the scrollview
    func initializeScrollView() {
        // set contentSize of scrollview: how much area it will scroll
        
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height*1.3)
        scrollView.showsVerticalScrollIndicator = true
        
        let statusBarHeight:CGFloat! = UIApplication.shared.statusBarFrame.size.height
        scrollView.frame = CGRect(x: 0, y: statusBarHeight, width: self.view.frame.width, height: self.view.frame.height)
        
        profileImage = UIImageView(frame:CGRect(x: 0, y: 0, width: screenWidth, height: screenWidth))
        
        photoUrl = teamMember.getProfileImage()

        let url = URL(string: photoUrl)
        profileImage.kf.setImage(with: url)
        
        Utils.shared.setViewToRoundedNoBorder(view: profileImage)
        scrollView.addSubview(profileImage)
        
        setDetailsTextView()
        
        self.view.addSubview(scrollView)
    }
    
    // checks if user has scrolled down a certain amount, will dismiss the view after a certain point.
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // pull down to dismiss
        let scrollOffset:CGFloat = scrollView.contentOffset.y
        if (scrollOffset < -40) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // sets the text view with the details
    func setDetailsTextView() {
        let detailsText:String! = teamMember.getDetailsText()
        
        detailsTextView.isEditable = false
        
        // disable scrolling: http://stackoverflow.com/questions/33978227/disable-scrolling-in-a-uitextview-while-avoiding-both-bouncing-to-top-and-invisi
        detailsTextView.isScrollEnabled = false
        
        // size to fit content:  http://stackoverflow.com/questions/50467/how-do-i-size-a-uitextview-to-its-content
        
        detailsTextView.frame = CGRect(x: 20, y: profileImage.frame.maxY + 10, width: screenWidth - 40, height: 400)
        
        detailsTextView.attributedText = Utils.shared.convertToAttributedString(str: detailsText)
        
        detailsTextView.frame.size = detailsTextView.sizeThatFits(detailsTextView.bounds.size)
        
        scrollView.addSubview(detailsTextView)
        
        resizeScrollView()
        
    }
    
    // resize the scrollview so it will be correct size with length of text 
    func resizeScrollView() {
        var sumLength:CGFloat! = detailsTextView.frame.height + profileImage.frame.height + UIApplication.shared.statusBarFrame.size.height + 50
        if (sumLength < screenHeight) {
            sumLength = screenHeight * 1.3
        }
        scrollView.contentSize = CGSize(width: screenWidth, height: sumLength)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
