//
//  TeamMember.swift
//  CMB_MeetTheTeam
//
//  Created by Paul Yang on 9/11/18.
//  Copyright © 2018 Paul Yang. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

// this represents a team member object

class TeamMember {
    
    // fields from json file 
    private var id:String = ""
    private var name:String = ""
    private var position:String = ""
    private var profile_image:String = ""
    private var personality:String = ""
    private var interests:String = ""
    private var dating_preferences:String = ""
    
    // constructor
    init(id:String, name:String, position:String, profile_image:String, personality:String, interests:String, dating_preferences:String) {
        self.id = id
        self.name = name
        self.position = position
        self.personality = personality
        self.profile_image = profile_image
        self.interests = interests
        self.dating_preferences = dating_preferences
    }
    
    // getters for fields
    func getId() -> String {
        return id
    }
    
    func getName() -> String {
        return name
    }
    
    func getPosition() -> String {
        return position
    }
    
    func getPersonality() -> String {
        return personality
    }
    
    func getProfileImage() -> String {
        return profile_image
    }
    
    func getInterests() -> String {
        return interests
    }
    
    func getDatingPreferences() -> String {
        return dating_preferences
    }
    
    // text for detailed card view
    func getDetailsText() -> String {
        let details:String = "name:\n" + getName() + "\n\nposition:\n" + getPosition() + "\n\npersonality: \n" + getPersonality() + "\n\ninterests: \n" + getInterests() + "\n\ndating preferences: \n" + getDatingPreferences()
        return details
    }
    
    // text for front card view, shorter
    func getShortDetailsText() -> String {
        let details:String = "name:  " + getName() + "\n\nposition:  " + getPosition()
        return details
    }
}
